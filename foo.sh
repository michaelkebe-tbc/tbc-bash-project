#!/usr/bin/env bash

output="foo"

while [[ -n "$1" ]]; do
  output+=" $1"
  shift
done

echo "$output"
