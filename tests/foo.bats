#!./test/libs/bats/bin/bats

load '../test/test_helper/bats-support/load'
load '../test/test_helper/bats-assert/load'

@test "can run script" {
    run ./foo.sh
}

@test "outputs foo" {
    run ./foo.sh
    assert_output foo
}

@test "outputs parameter" {
    run ./foo.sh bar
    assert_output "foo bar"
}

@test "outputs another parameter" {
    run ./foo.sh baz
    assert_output "foo baz"
}

@test "two parameters" {
    run ./foo.sh bar baz
    assert_output "foo bar baz"
}

@test "many parameters" {
    run ./foo.sh 1 2 3 4 5 6 7 8 9 10
    assert_output "foo 1 2 3 4 5 6 7 8 9 10"
}
